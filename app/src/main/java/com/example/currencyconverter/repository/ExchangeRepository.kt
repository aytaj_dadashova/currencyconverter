package com.example.currencyconverter.repository

import android.util.Log
import androidx.room.withTransaction
import com.example.currencyconverter.data.pojos.ExchangePOJO
import com.example.currencyconverter.local.ExchangeDatabase
import com.example.currencyconverter.network.ApiInitHelper
import com.example.currencyconverter.network.NetworkState
import com.example.currencyconverter.utils.checkNetworkRequestResponse
import com.example.currencyconverter.utils.exchangeAsEntityList
import retrofit2.Response
import java.io.IOException
import java.lang.Exception

class ExchangeRepository(private val database: ExchangeDatabase) {

    // get all list from DAO
    val listOfRates get() = database.exchangeRateDao.getExchangeRates()

    private val exchangeRateService = ApiInitHelper.exchangeService

    suspend fun fetchRates(base: String = ""): NetworkState = try {

        val response = exchangeRateService.getExchangeRates(base)

        checkNetworkRequestResponse(response).also {
            if (it is NetworkState.Success<*>) {
                val data = response.body() ?: listOf()

                database.withTransaction {

                    // delete and reInsert all data
                    database.exchangeRateDao.deleteAll()
                    database.exchangeRateDao.insertAll(data.exchangeAsEntityList())

                }
            }
        }

    } catch (e: IOException) {
        NetworkState.NetworkException(e.message)
    }


}





