package com.example.currencyconverter.utils

/**
 * Created by Aytaj on 7/4/2020
 */
sealed class UiState

object Loading : UiState()
object HasData : UiState()
object NoData : UiState()