package com.example.currencyconverter.utils

/**
 * Created by Aytaj on 7/3/2020
 */
//generic recycler click
class RecyclerViewItemClickListener<T>(val itemClickListener: (data: T?) -> Unit) {
    fun onItemClick(data: T) = itemClickListener(data)
}