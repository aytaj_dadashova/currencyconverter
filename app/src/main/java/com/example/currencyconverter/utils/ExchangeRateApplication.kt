package com.example.currencyconverter.utils

import android.app.Application
import timber.log.Timber

class ExchangeRateApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }

}