package com.example.currencyconverter.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.example.currencyconverter.data.pojos.ExchangePOJO
import com.example.currencyconverter.local.ExchangeRateEntity

fun ViewModel.checkConnectivity(context: Context): Boolean {
    var result = false
    val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val networkCapabilities = cm.activeNetwork ?: return false
        val actNw =
            cm.getNetworkCapabilities(networkCapabilities) ?: return false
        result = when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    } else {
        cm.run {
            cm.activeNetworkInfo?.run {
                result = when (type) {
                    ConnectivityManager.TYPE_WIFI -> true
                    ConnectivityManager.TYPE_MOBILE -> true
                    ConnectivityManager.TYPE_ETHERNET -> true
                    else -> false
                }

            }
        }
    }
    return result
}

// create toast
fun Context.Toast(message: String?) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

// convert pojo objects to room entity
fun List<ExchangePOJO>.exchangeAsEntityList() = map{

    ExchangeRateEntity(
        code=it.code,
        alphaCode = it.alphaCode,
        date = it.date,
        inverseRate = it.inverseRate,
        name = it.name,
        numericCode = it.numericCode,
        rate = it.rate

    )
}