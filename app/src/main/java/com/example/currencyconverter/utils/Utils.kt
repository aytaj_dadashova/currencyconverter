package com.example.currencyconverter.utils

import com.example.currencyconverter.network.NetworkState
import retrofit2.Response

fun <T> checkNetworkRequestResponse(response: Response<T>?): NetworkState {
    return if (response?.isSuccessful == true) {

        response.body()?.let { data ->

            if (response.code() == 200) NetworkState.Success(data)
            else NetworkState.HandledHttpError(response.message())
        } ?: NetworkState.InvalidData
    } else NetworkState.UnhandledHttpError(response?.message() + response?.code())
}

