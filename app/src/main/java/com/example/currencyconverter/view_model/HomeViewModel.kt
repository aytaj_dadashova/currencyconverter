package com.example.currencyconverter.view_model

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.example.currencyconverter.R
import com.example.currencyconverter.data.pojos.ExchangePOJO
import com.example.currencyconverter.local.ExchangeRateEntity
import com.example.currencyconverter.local.getDatabase
import com.example.currencyconverter.network.NetworkState
import com.example.currencyconverter.repository.ExchangeRepository
import com.example.currencyconverter.utils.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class HomeViewModel(application: Application) : AndroidViewModel(application) {


    private val context = application.applicationContext
    private val exchangeRepository = ExchangeRepository(getDatabase(context))
    private var refreshValue = 1

    // get cached data refresh it's live data with new rates

    val listOfRates =
            Transformations.map(exchangeRepository.listOfRates ){

                it.map {
                    it.apply {
                        rate*=refreshValue

                    }
                }
            }

    //without database
    private val _exchangeData = MutableLiveData<List<ExchangePOJO>?>()
    val exchangeData: LiveData<List<ExchangePOJO>?>
        get() = _exchangeData

    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState

   fun refresValue(amount: String) {

       refreshValue = amount.toInt()

   }

    @Suppress("UNCHECKED_CAST")
    fun getExchangeRates(base: String) = viewModelScope.launch(Dispatchers.IO) {
        if (checkConnectivity(context)) {
            withContext(Dispatchers.Main) {
                _uiState.value = Loading
            }
            when (val response = exchangeRepository.fetchRates(base)) {
                is NetworkState.Success<*> -> {
                    withContext(Dispatchers.Main) {
                        _uiState.value = HasData

                        // without room database just getting data from api
//                        val data = response.data as List<ExchangePOJO>
//                        _exchangeData.postValue(data)
                    }
                }
                is NetworkState.HandledHttpError -> handleError(response.error)

                is NetworkState.UnhandledHttpError -> handleError(response.error)

                is NetworkState.NetworkException -> handleError(response.exception)
            }
        } else {
            handleError(context.getString(R.string.error_no_internet))
        }
    }

    private suspend fun handleError(message: String?) {
        withContext(Dispatchers.Main) {
            _uiState.value = NoData

            context.Toast(message)
        }
    }

}