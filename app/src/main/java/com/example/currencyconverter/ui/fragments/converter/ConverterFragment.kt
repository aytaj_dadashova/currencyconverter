package com.example.currencyconverter.ui.fragments.converter

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.currencyconverter.R
import com.example.currencyconverter.databinding.FragmentConverterBinding
import com.example.currencyconverter.view_model.ConverterViewModel
import kotlinx.android.synthetic.main.fragment_converter.*


class ConverterFragment : Fragment() {

    private val converterViewModel: ConverterViewModel by viewModels()
    private val args: ConverterFragmentArgs by navArgs()
    private lateinit var binding: FragmentConverterBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        bindUI()
        bindArguments()
        setClickListeners()
    }

    private fun setClickListeners() {
        button_ok.setOnClickListener {

            findNavController().navigate(
                ConverterFragmentDirections.actionConverterFragmentToHomeFragment(
                    edit_text_number.text.toString(), args.exchangeCode
                )
            )
        }
    }

    private fun bindArguments() {
        val code = args.exchangeCode
        text_selected_code.text = code
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentConverterBinding.inflate(inflater)
        return binding.root

    }

    private fun bindUI(): Unit = with(binding) {

        lifecycleOwner = this@ConverterFragment
        viewModel = converterViewModel

    }


}