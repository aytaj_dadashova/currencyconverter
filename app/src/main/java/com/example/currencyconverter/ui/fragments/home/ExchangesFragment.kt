package com.example.currencyconverter.ui.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.currencyconverter.databinding.FragmentHomeBinding
import com.example.currencyconverter.utils.DEFAULT_BASE
import com.example.currencyconverter.utils.DEFAULT_VALUE
import com.example.currencyconverter.utils.RecyclerViewItemClickListener
import com.example.currencyconverter.view_model.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*

class ExchangesFragment : Fragment() {

    private val homeViewModel: HomeViewModel by viewModels()
    private val amountArgs: ExchangesFragmentArgs by navArgs()

    private lateinit var adapter: ExchangeAdapter

    private lateinit var binding: FragmentHomeBinding

    private var selectedCode: String? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindUi()

        initRecyclerView()

        observeValue()
    }

    private fun bindUi(): Unit = with(binding) {
        lifecycleOwner = this@ExchangesFragment
        viewModel = homeViewModel
    }

    private fun initRecyclerView(): Unit = with(binding) {
        adapter = ExchangeAdapter(RecyclerViewItemClickListener {
            setClickListener(it!!.code)

        })
        recycler_view_exchanges.adapter = adapter

    }


    private fun observeValue(): Unit = with(homeViewModel) {
        //observe cached data

        listOfRates.observe(viewLifecycleOwner, Observer {
            it?.let { exchangeRates ->
                adapter.submitList(exchangeRates)
            }
        })


    }

    override fun onStart() {
        super.onStart()

        if (amountArgs.selectedCode != DEFAULT_VALUE) {

            homeViewModel.getExchangeRates(selectedCode.toString())
            homeViewModel.refresValue(amountArgs.rateNum)

        } else {


            homeViewModel.getExchangeRates(DEFAULT_BASE)


        }
    }

    private fun setClickListener(code: String?) {

        selectedCode = code

        findNavController().navigate(
            ExchangesFragmentDirections
                .actionHomeFragmentToConverterFragment(selectedCode!!)
        )


    }

}