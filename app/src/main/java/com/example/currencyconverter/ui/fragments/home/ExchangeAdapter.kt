package com.example.currencyconverter.ui.fragments.home



import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.example.currencyconverter.data.pojos.ExchangePOJO
import com.example.currencyconverter.local.ExchangeRateEntity
import com.example.currencyconverter.utils.RecyclerViewItemClickListener


class ExchangeAdapter(private val itemClickListener: RecyclerViewItemClickListener<ExchangeRateEntity>
    ) : ListAdapter<ExchangeRateEntity, ExchangesViewHolder>(ExchangesDiffCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExchangesViewHolder {
        return ExchangesViewHolder.fromParent(parent)
    }

    override fun onBindViewHolder(holder: ExchangesViewHolder, position: Int) {
        holder.bind(getItem(position), itemClickListener)
    }


}