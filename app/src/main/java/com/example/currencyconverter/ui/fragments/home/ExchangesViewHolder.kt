package com.example.currencyconverter.ui.fragments.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.currencyconverter.databinding.ItemExchangeListBinding
import com.example.currencyconverter.local.ExchangeRateEntity
import com.example.currencyconverter.utils.RecyclerViewItemClickListener


class ExchangesViewHolder private constructor(val binding: ItemExchangeListBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(
        exchangesItem: ExchangeRateEntity,
        recyclerViewItemClickListener: RecyclerViewItemClickListener<ExchangeRateEntity>
    )
            : Unit = with(binding) {
        exchangeItemData = exchangesItem
        root.setOnClickListener {
            recyclerViewItemClickListener.onItemClick(exchangesItem)
        }

    }

    companion object {

        fun fromParent(parent: ViewGroup): ExchangesViewHolder {

            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemExchangeListBinding.inflate(inflater, parent, false)

            return ExchangesViewHolder(binding)
        }
    }


}