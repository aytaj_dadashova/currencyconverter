package com.example.currencyconverter.ui.fragments.home

import androidx.recyclerview.widget.DiffUtil
import com.example.currencyconverter.data.pojos.ExchangePOJO
import com.example.currencyconverter.local.ExchangeRateEntity


/**
 * Created by Aytaj on 3/7/2020
 */
object ExchangesDiffCallback : DiffUtil.ItemCallback<ExchangeRateEntity>() {

    override fun areItemsTheSame(
        oldItem: ExchangeRateEntity,
        newItem: ExchangeRateEntity
    ): Boolean = (oldItem.code == newItem.code)

    override fun areContentsTheSame(
        oldItem: ExchangeRateEntity,
        newItem: ExchangeRateEntity
    ): Boolean = (oldItem == newItem)

}
