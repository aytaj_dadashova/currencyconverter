package com.example.currencyconverter.network


import com.example.currencyconverter.data.pojos.ExchangePOJO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    interface ExchangeRateService {

        @GET("rates.php")
        suspend fun getExchangeRates(
            @Query("base") base: String)
                : Response<List<ExchangePOJO>>

    }

}