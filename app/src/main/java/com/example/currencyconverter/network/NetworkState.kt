package com.example.currencyconverter.network

sealed class NetworkState {

    data class Success<T>(val data: T) : NetworkState()

    object InvalidData : NetworkState()

    data class HandledHttpError(val error: String?) : NetworkState()
    data class UnhandledHttpError(val error: String?) : NetworkState()

    data class NetworkException(val exception: String?) : NetworkState()
}