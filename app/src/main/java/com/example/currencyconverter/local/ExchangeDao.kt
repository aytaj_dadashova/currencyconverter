package com.example.currencyconverter.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.currencyconverter.data.pojos.ExchangePOJO


@Dao
interface ExchangeDao {

    @Query("select * from exchangeRates")
    fun getExchangeRates(): LiveData<List<ExchangeRateEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(exchangeRate: ExchangeRateEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items : List<ExchangeRateEntity>)

    @Query("delete from exchangeRates")
    suspend fun deleteAll()

    @Query("delete from exchangeRates where code like :code")
    suspend fun delete(code: String)


}