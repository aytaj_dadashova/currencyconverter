package com.example.currencyconverter.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [ExchangeRateEntity::class], version = 1)
abstract class ExchangeDatabase : RoomDatabase() {
    abstract val exchangeRateDao: ExchangeDao
}

private const val DATABASE_NAME = "ExchangeRates"
private lateinit var INSTANCE: ExchangeDatabase

fun getDatabase(context: Context): ExchangeDatabase {
    synchronized(ExchangeDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(
                context.applicationContext,
                ExchangeDatabase::class.java,
                DATABASE_NAME
            ).build()
        }
    }
    return INSTANCE
}